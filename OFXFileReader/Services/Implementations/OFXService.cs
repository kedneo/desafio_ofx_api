﻿using DataAccess.Repositories.Interfaces;
using Entities;
using Entities.Helpers.Interfaces;
using OFXFileReader.Business.Interfaces;
using OFXFileReader.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OFXFileReader.Services.Implementations
{
    public class OFXService : IOFXService
    {
        public IFileReader OFXFileReader { get; }
        public IOFXTransactionHelper OfxTransactionHelper { get; }
        public IOFXRepository TransactionsOFXRepository { get; }

        public OFXService(
            IFileReader ofxFileReader,
            IOFXTransactionHelper ofxTransactionHelper,
            IOFXRepository transactionsOFXRepository)
        {
            OFXFileReader = ofxFileReader ??
                throw new ArgumentNullException(nameof(ofxFileReader));
            OfxTransactionHelper = ofxTransactionHelper ??
                throw new ArgumentNullException(nameof(ofxTransactionHelper));
            TransactionsOFXRepository = transactionsOFXRepository ??
                throw new ArgumentNullException(nameof(transactionsOFXRepository));
        }

        public IEnumerable<OFXTransaction> GetAll()
        {
            return TransactionsOFXRepository.GetAll().ToList().OrderByDescending(x => x.DateTime);
        }

        public async Task SaveOFXTransactions(IEnumerable<Stream> fileListAsStream)
        {
            var OFXEntityList = new List<OFXEntity>();
            foreach (var stream in fileListAsStream)
            {
                OFXEntityList.Add(await ReadFile(stream));
            }

            var transactionsMerged = new List<OFXTransaction>();
            foreach (var ofxFile in OFXEntityList)
            {
                if (transactionsMerged.Count > 0)
                {
                    transactionsMerged = OfxTransactionHelper.Merge(transactionsMerged, ofxFile.OFXTransactions)
                                            .ToList();
                }
                else
                {
                    transactionsMerged = ofxFile.OFXTransactions.ToList();
                }
            }

            var transactionsStored = GetAll();
            if (transactionsStored != null)
            {
                transactionsMerged = OfxTransactionHelper.Merge(transactionsStored, transactionsMerged).ToList();
            }
            TransactionsOFXRepository.Save(transactionsMerged);
        }

        private async Task<OFXEntity> ReadFile(Stream stream)
        {
            return await OFXFileReader.ReadFile(stream);
        }

    }
}
