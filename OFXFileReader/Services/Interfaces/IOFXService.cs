﻿using Entities;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace OFXFileReader.Services.Interfaces
{
    public interface IOFXService
    {

        IEnumerable<OFXTransaction> GetAll();

        Task SaveOFXTransactions(IEnumerable<Stream> fileListAsStream);

    }
}
