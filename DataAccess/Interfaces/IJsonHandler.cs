﻿using Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataAccess.Interfaces
{
    public interface IJsonHandler
    {
        IEnumerable<OFXTransaction> GetInstance(string json);
        string Serialize(IEnumerable<OFXTransaction> obj);
        void SaveData(string path, string fileContent);
        string ReadFile(string path);
    }
}
