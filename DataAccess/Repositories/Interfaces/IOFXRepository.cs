﻿using Entities;
using System.Collections.Generic;

namespace DataAccess.Repositories.Interfaces
{
    public interface IOFXRepository
    {

        void Save(IEnumerable<OFXTransaction> entity);

        IEnumerable<OFXTransaction> GetAll();

    }
}
